


let x = 5
let y = 6

//Addition Operator
let sum = x + y
console.log("Result of addition operator: " + sum)


//Subtraction Operator
let difference = x-y
console.log("Result of subtraction operator: " + difference)


//Multiplication Operator
let product = x * y
console.log("Result of the multiplication operator: " + product)

//Division Operator
let quotient = x / y
console.log("Result of the division operator: " + quotient)

//Modulo Operator
let remainder = x % y
console.log("Result of modulo operator: " + remainder)

//Assignment Operator
//Basic Assignment Operator (=)
//The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8

//Addition Assignment Operator
assignmentNumber += 2
console.log("Result of the addition assignment operator: " + assignmentNumber)

//Subtraction/Multiplication/Divistion (-=,*=,/=)

//Subtraction Assignment Operator
assignmentNumber -= 2
console.log("Result of the subtraction assignment operator: " + assignmentNumber)

//Multiplication Assignment Operator
assignmentNumber *= 2
console.log("Result of the multiplication assignment operator: " + assignmentNumber)

//Division Assignment Operator
assignmentNumber /= 2
console.log("Result of the division assignment operator: " + assignmentNumber)


//Multiple Operators and Parentheses
/*
	PEMDAS Rules


*/

let mdas = 1 + 2 - 3 * 4 / 5
console.log("Result of the mdas operation: " + mdas)

let pemdas = 1 + (2 - 3) * (4 / 5)
console.log("Result of pemdas operation: " + pemdas)

let sample = 5 % 2 * 10;
console.log(sample)


//Increment and Decrement 
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

//Pre-increment
let z = 1
let preIncrement = ++z

console.log("Result of the pre-increment: " + preIncrement)

console.log("Result of the pre-increment: " + z)

//Post-increment

z = 1

let postIncrement = z++

console.log("Result of the post-increment: " + postIncrement)
console.log("Result of the post-increment: " + z)


let a = 2

//Pre-Decrement
let preDecrement = --a
console.log("Result of the pre-decrement: " + preDecrement)
console.log("Result of the pre-decrement: " + a)


//Post-Decrement
a=2
let postDecrement = a--
console.log("Result of the post-decrement: " + postDecrement)
console.log("Result of the post-decrement: "  + a)


//Type Coercion

let numA = '10'
let numB = 12



let coercion = numA + numB 
console.log(coercion)
console.log(typeof coercion)



//Non-coercion

let numC = 16
let numD = 14;

let nonCoercion = numC + numD
console.log(nonCoercion)
console.log(typeof nonCoercion)

//Addtion of Number and Boolean

let numE = true + 1
console.log(numE)
console.log(typeof numE)


//Comparison Operator
let juan = 'juan'


//Equality Operator (==)

console.log(1==1)
console.log(1==2)
console.log(1=='1')
console.log(1== true)

console.log('juan' == 'juan')
console.log(juan == 'juan')


//Inequality Operator


console.log(1 != 1) 
console.log(1 != 2)
console.log(1 !=  '1')
console.log(1 != true)
console.log('juan' != 'juan')
console.log('juan' != juan)


//Strict Equality Operator(===)

/*
	-Checks whether the operands are equal/have the same content
	-Also COMPARES the data types of 2 values


*/

console.log(1===1)
console.log(1 === 2)
console.log(1 ===  '1')
console.log(1 === true)
console.log('juan' === 'juan')
console.log('juan' === juan)


//Relational Operator
let j = 50
let k = 75

let isGreaterThan = j > k
console.log(isGreaterThan)

let isLessThan = j < k
console.log(isLessThan)


let isGTorEqual = j >= k
console.log(isGTorEqual)

let isLTorEqual = j<=k
console.log(isLTorEqual)


//Logical Operators
let isLegalAge = true
let isRegistered = false


//Logical AND Operator (&&)

let allRequirementsMet = isLegalAge && isRegistered
console.log(allRequirementsMet)

//Logical OR Operator (||)
let someRequirementsMet = isLegalAge || isRegistered
console.log(someRequirementsMet)

//Logical NOT Operator (!)
let someRequirementsNotMet = !isRegistered
console.log(someRequirementsNotMet)













